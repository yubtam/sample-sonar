package org.sonar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.sonar.MessageBuilder;

public class MessageBuilderTests {
	
    @Test
    public void testgetMessageNotNull() {
        MessageBuilder obj = new MessageBuilder();
        assertEquals("Hello Yubraj", obj.getMessage("Yubraj"));

    }
    
    @Test
    public void testgetMessageNull() {
        MessageBuilder obj = new MessageBuilder();
        assertEquals("Please provide a name!", obj.getMessage(" "));
    }
    
    @Test
    public void testgetMessageEmpty() {
    	MessageBuilder obj = new MessageBuilder();
    	assertEquals("Please provide a name!", obj.getMessage(null));
    }
    
}
