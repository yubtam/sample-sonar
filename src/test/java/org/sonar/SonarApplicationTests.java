package org.sonar;


import org.junit.jupiter.api.Test;
import org.sonar.SonarApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SonarApplicationTests {

	@Test
	public void testMain() {
		SonarApplication.main(new String[] {});
    }

}
